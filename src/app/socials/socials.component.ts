import { Component } from '@angular/core';
import {
  faGitlab,
  faGithub,
  faLinkedin,
} from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-socials',
  templateUrl: './socials.component.html',
  styleUrls: ['./socials.component.scss'],
})
export class SocialsComponent {
  faGitlab = faGitlab;
  faGithub = faGithub;
  faLinkedin = faLinkedin;
}
